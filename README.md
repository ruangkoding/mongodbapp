# MONGODBAPP

Aplikasi **mongodbapp** merupakan sebuah aplikasi _microservice_ yang bertanggung jawab dalam mengolah data buku perpustakaan.

---

## Setup
Port menggunakan default dari FastAPI, yaitu port: `8000`.

---

## Dokumentasi API

1. Mengambil data semua buku<br>
**GET** `localhost:8000/books` <br>
2. Mengambil data buku berdasarkan id <br>
**GET** `localhost:8000/bookbyid` <br>
    - Body <br>
        - raw JSON <br> 
            ```
            {
                "id":"_id bukunya_"
            }
            ```

3. Mengambil data buku berdasarkan nama <br>
**GET** `localhost:8000/bookbyname`<br>
    - Body<br>
        - raw JSON <br>
            ```
            {
                "nama":"_nama bukunya_"
            }   
            ```

---
## ERD

* dapat di akses pada berkas: `erd.jpg`.

## Biodata

* Dapat di akses pada berkas: `biodata.txt`.
